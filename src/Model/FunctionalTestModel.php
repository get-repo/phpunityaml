<?php

namespace GetRepo\PHPUnitYaml\Model;

use GetRepo\PHPUnitYaml\Configuration\FunctionalTestConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * FunctionalTestModel.
 */
class FunctionalTestModel extends AbstractModel
{
    public static function getConfigurationClassName(): ConfigurationInterface
    {
        return new FunctionalTestConfiguration();
    }
}
