<?php

namespace GetRepo\PHPUnitYaml\Model;

use GetRepo\PHPUnitYaml\Util\ParameterBagUtil;
use ReflectionMethod;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\PropertyAccess as PA;

/**
 * AbstractModel.
 */
abstract class AbstractModel implements ModelInterface
{
    private ?string $filePath = null;

    private readonly array $values;

    abstract public static function getConfigurationClassName(): ConfigurationInterface;

    /**
     * @param mixed[] $values
     */
    public function __construct(private readonly string $name, array $values)
    {
        if (isset($values['yaml_file'])) {
            $this->filePath = $values['yaml_file'];
            unset($values['yaml_file']);
        }
        $this->values = $values;
    }

    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getYamlFilePath(): ?string
    {
        return $this->filePath;
    }

    /**
     * @return mixed
     */
    public function get(string $path = null, bool $enableExceptionOnInvalidIndex = true)
    {
        $propertyAccessor = PA\PropertyAccess::createPropertyAccessorBuilder();
        if ($enableExceptionOnInvalidIndex) {
            $propertyAccessor->enableExceptionOnInvalidIndex();
        } else {
            $propertyAccessor->disableExceptionOnInvalidIndex();
        }

        $value = $this->values;
        if (!is_null($path)) {
            $value = $propertyAccessor->getPropertyAccessor()->getValue($this->values, $path);
        }

        // Parameter bag does not handle object values, so we handle it manually
        ParameterBagUtil::getIfObject($value);

        return $value;
    }

    public function getReflectionMethod(): ReflectionMethod
    {
        return new ReflectionMethod(
            $this->get('[class]'),
            $this->get('[method]')
        );
    }
}
