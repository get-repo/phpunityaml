<?php

namespace GetRepo\PHPUnitYaml\Model;

use GetRepo\PHPUnitYaml\Configuration\UnitTestConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * UnitTestModel.
 */
class UnitTestModel extends AbstractModel
{
    use ReflectionTrait;

    public static function getConfigurationClassName(): ConfigurationInterface
    {
        return new UnitTestConfiguration();
    }
}
