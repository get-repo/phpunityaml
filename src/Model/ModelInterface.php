<?php

namespace GetRepo\PHPUnitYaml\Model;

use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * ModelInterface.
 */
interface ModelInterface
{
    public static function getConfigurationClassName(): ConfigurationInterface;

    /** @return mixed */
    public function get(string $path = null, bool $enableExceptionOnInvalidIndex = true);
}
