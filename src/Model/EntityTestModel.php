<?php

namespace GetRepo\PHPUnitYaml\Model;

use GetRepo\PHPUnitYaml\Configuration\EntityTestConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * EntityTestModel.
 */
class EntityTestModel extends AbstractModel
{
    use ReflectionTrait;

    public static function getConfigurationClassName(): ConfigurationInterface
    {
        return new EntityTestConfiguration();
    }
}
