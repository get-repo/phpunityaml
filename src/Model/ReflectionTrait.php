<?php

namespace GetRepo\PHPUnitYaml\Model;

use ReflectionClass;
use ReflectionMethod;

/**
 * ReflectionTrait.
 */
trait ReflectionTrait
{
    public function getReflectionClass(): ReflectionClass
    {
        return new ReflectionClass($this->get('[class]'));
    }

    public function getReflectionMethod(): ReflectionMethod
    {
        return new ReflectionMethod($this->get('[class]'), $this->get('[method]'));
    }
}
