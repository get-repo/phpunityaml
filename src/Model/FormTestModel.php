<?php

namespace GetRepo\PHPUnitYaml\Model;

use GetRepo\PHPUnitYaml\Configuration\FormTestConfiguration;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * FormTestModel.
 */
class FormTestModel extends AbstractModel
{
    public static function getConfigurationClassName(): ConfigurationInterface
    {
        return new FormTestConfiguration();
    }
}
