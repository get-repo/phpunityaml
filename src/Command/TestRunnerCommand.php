<?php

namespace GetRepo\PHPUnitYaml\Command;

use GetRepo\PHPUnitYaml\PHPUnit\Printer;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidOptionException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * TesrRunnerCommand.
 */
class TestRunnerCommand extends Command
{
    use ContainerAwareTrait;

    /** @var string */
    final public const NAME = 'phpunityaml:run';

    private array $phpUnitArgs = [];

    /**
     * Catching PHPUnit options.
     *
     * {@inheritdoc}
     *
     * @see Command::run()
     */
    public function run(InputInterface $input, OutputInterface $output): int
    {
        $args = $_SERVER['argv'];
        $definition = $this->getDefinition();
        foreach ($args as $key => $arg) {
            $match = [];
            if (preg_match('/^(\-\-?)([^$]+)$/', (string) $arg, $match)) {
                if ('-' === $match[1]) {
                    $name = $arg[1];
                } else { // options like --help
                    $name = explode('=', $match[2])[0];
                }
                if (!$definition->hasOption($name) && !$definition->hasShortcut($name)) {
                    $this->phpUnitArgs[] = $arg;
                    unset($args[$key]);
                }
            }
        }

        $classInput = $input::class;
        $input = new $classInput(array_merge(['command'], $args));

        return parent::run($input, $output);
    }

    /**
     * {@inheritdoc}
     *
     * @see Command::configure()
     */
    protected function configure(): void
    {
        $this
            ->setName(self::NAME)
            ->setDescription('PHPUnitYAML Test Runner.')
            ->addArgument(
                'path',
                InputArgument::OPTIONAL,
                'Path to YAML test files'
            )
            ->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'Test type filter'
            )
            ->addOption(
                'file',
                'f',
                InputOption::VALUE_REQUIRED,
                'File filter'
            )
        ;
    }

    /**
     * {@inheritdoc}
     *
     * @see Command::initialize()
     */
    protected function initialize(InputInterface $input, OutputInterface $output): void
    {
        // validate test path
        $path = $input->getArgument('path');
        if ($path && !file_exists($path)) {
            throw new InvalidOptionException("Invalid path '{$path}'");
        }

        // validate type
        $type = $input->getOption('type');
        if ($type && !$this->container->has('phpunityaml.runner.' . strtolower((string) $type))) {
            $message = sprintf(
                "Invalid type filter '%s'. Valid types are: %s",
                $type,
                implode(', ', $this->container->getParameter('phpunityaml.runners'))
            );
            throw new InvalidOptionException($message);
        }
    }

    /**
     * {@inheritdoc}
     *
     * @see Command::execute()
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $path = $input->getArgument('path');
        $xml = sprintf('-c%s/phpunit.xml', APP_ROOT_PATH);
        $typeFilter = $input->getOption('type');
        $fileFilter = $input->getOption('file');
        $found = $failed = false;

        foreach ($runners = $this->container->getParameter('phpunityaml.runners') as $key => $runnerName) {
            // apply filter type
            if ($typeFilter && $runnerName !== $typeFilter) {
                continue;
            }

            /** @var \GetRepo\PHPUnitYaml\Runner\AbstractRunner $runner */
            $runner = $this->container->get("phpunityaml.runner.{$runnerName}");

            if (!$runner->canBeExecuted()) {
                unset($runners[$key]); // remove runner from not found message list below
                continue;
            }

            if ($fileFilter) {
                $runner->setFileFilter($fileFilter);
            }

            if ($path) {
                $runner->setTestPath($path);
            }

            if (!$runner->getTestFiles()) {
                continue;
            }

            $found = true;
            $output->writeln('');
            $output->writeln(
                sprintf('<bg=yellow;options=bold> %s tests </>', ucfirst((string) $runnerName))
            );

            $options = [];
            if (\file_exists($xml)) {
                $options[] = $xml;
            }

            $phpunitArgv = \array_merge(
                $options,
                [
                    '--printer=' . Printer::class,
                    '--colors=always',
                    (new \ReflectionClass($runner))->getFileName(),
                ],
                $this->phpUnitArgs // args at the end for phpunit args errors
            );

            $phpunitCommand = new \PHPUnit\TextUI\Command();
            $code = $phpunitCommand->run($phpunitArgv, false);
            if ($code > 0) {
                $failed = true;
            }
            $output->writeln('');
        }

        if (!$found) {
            $output->writeln(
                \sprintf('<error> No tests were found for types \'%s\' </>', implode("', '", $runners))
            );

            return 1; // error status code
        }

        return $failed ? 1 : 0; // success status code
    }
}
