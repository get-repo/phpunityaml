<?php

namespace GetRepo\PHPUnitYaml\Configuration;

use GetRepo\PHPUnitYaml\DependencyInjection\GetRepoPHPUnitYamlExtension;
use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder(): Builder\TreeBuilder
    {
        $treeBuilder = new Builder\TreeBuilder(GetRepoPHPUnitYamlExtension::ALIAS);
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->children()
                ->arrayNode('functional')
                    ->children()
                        ->arrayNode('server_parameters')
                            ->scalarPrototype()->end()
                        ->end()
                    ->end()
                ->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
