<?php

namespace GetRepo\PHPUnitYaml\Configuration;

use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Form\FormTypeInterface;

class FormTestConfiguration extends AbstractTestConfiguration
{
    public function getConfigTreeBuilder(): \Symfony\Component\Config\Definition\Builder\TreeBuilder
    {
        $treeBuilder = new Builder\TreeBuilder('form');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            // merge values in tests
            ->beforeNormalization()
                ->ifTrue(fn ($values) =>
                    // if init and tests
                    isset($values['tests']) && is_array($values['tests']) && $values['tests'])
                ->then(function ($values) {
                    $init = isset($values['init']) && is_array($values['init']) ? $values['init'] : [];
                    foreach ($values['tests'] as &$tests) {
                        foreach ($tests as &$test) {
                            // set classname
                            $test['class'] = $values['class'] ?? null;

                            // merge values
                            foreach (['variables', 'mocks'] as $key) {
                                if (isset($init[$key])) {
                                    $test[$key] = array_merge(
                                        $init[$key],
                                        ($test[$key] ?? [])
                                    );
                                }
                            }
                        }
                    }

                    return $values;
                })
            ->end()
            ->canBeDisabled()
            ->children()
                // class
                ->scalarNode('class')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->validate()
                        ->ifTrue(
                            fn ($class) => !class_exists($class) || !is_subclass_of($class, FormTypeInterface::class)
                        )
                        ->thenInvalid(
                            'Class %s does not exists or not subclass of ' . FormTypeInterface::class
                        )
                    ->end()
                ->end()
                ->arrayNode('init')
                    ->children()
                        ->append($this->getVariablesNode())
                        ->append($this->getMocksNode())
                    ->end()
                ->end()
                ->arrayNode('tests')
                    ->prototype('array')
                        ->prototype('array')
                            ->canBeDisabled()
                            ->children()
                                ->scalarNode('class') // internal only
                                    ->isRequired()
                                    ->cannotBeEmpty()
                                ->end()
                                ->arrayNode('options')
                                    ->defaultValue([])
                                    ->prototype('variable')->end()
                                ->end()
                                ->variableNode('data')
                                    ->defaultNull()
                                ->end()
                                ->arrayNode('variables')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('mocks')
                                    ->prototype('variable')->end()
                                ->end()
                                ->append($this->getAssertNode())
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('disabled')
                    ->prototype('variable')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
