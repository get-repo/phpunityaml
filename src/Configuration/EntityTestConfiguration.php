<?php

namespace GetRepo\PHPUnitYaml\Configuration;

use Symfony\Component\Config\Definition\Builder;

class EntityTestConfiguration extends AbstractTestConfiguration
{
    public function getConfigTreeBuilder(): \Symfony\Component\Config\Definition\Builder\TreeBuilder
    {
        $treeBuilder = new Builder\TreeBuilder('entity');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            // merge init values in tests
            ->beforeNormalization()
                ->ifTrue(fn ($values) =>
                    // if init and tests
                    isset($values['tests']) && is_array($values['tests']) && $values['tests'])
                ->then(function ($values) {
                    // merge init values in each tests assert
                    $init = isset($values['init']) && is_array($values['init']) ? $values['init'] : [];
                    foreach ($values['tests'] as $method => &$tests) {
                        foreach ($tests as &$test) {
                            // set classname
                            $test['class'] = $values['class'] ?? null;
                            // set method
                            $test['method'] = $method;
                            // default value if not set
                            foreach (['constructor'] as $key) {
                                if (!isset($test[$key]) && isset($init[$key])) {
                                    $test[$key] = $init[$key];
                                }
                            }

                            // merge values
                            foreach (['variables', 'mocks', 'data'] as $key) {
                                if (isset($init[$key])) {
                                    $test[$key] = array_merge(
                                        $init[$key],
                                        ($test[$key] ?? [])
                                    );
                                }
                            }
                        }
                    }

                    return $values;
                })
            ->end()
            ->canBeDisabled()
            ->children()
                // class
                ->scalarNode('class')
                    ->isRequired()
                    ->cannotBeEmpty()
                    ->validate()
                        ->ifTrue(fn ($class) => !class_exists($class) && !interface_exists($class))
                        ->thenInvalid('Class %s does not exists')
                    ->end()
                ->end()
                ->booleanNode('strict')
                    ->defaultFalse()
                ->end()
                ->arrayNode('init')
                    ->children()
                        ->arrayNode('constructor')
                            ->prototype('variable')->end()
                        ->end()
                        ->append($this->getVariablesNode())
                        ->append($this->getMocksNode())
                        ->append($this->getDataNode())
                    ->end()
                ->end()
                // tests
                ->arrayNode('tests')
                    ->prototype('array') // methods
                        ->prototype('array') // method test
                            ->canBeDisabled()
                            ->children()
                                ->arrayNode('constructor')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('variables')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('parameters')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('mocks')
                                    ->prototype('variable')->end()
                                ->end()
                                ->arrayNode('data')
                                    ->prototype('variable')->end()
                                ->end()
                                ->scalarNode('class')->end()
                                ->scalarNode('method')->end()
                                ->append($this->getAssertNode())
                            ->end()
                        ->end()
                    ->end()
                ->end()
                // disabled
                ->arrayNode('disabled')
                    ->prototype('variable')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
