<?php

namespace GetRepo\PHPUnitYaml\Configuration;

use Symfony\Component\Config\Definition\Builder;

class FunctionalTestConfiguration extends AbstractTestConfiguration
{
    public function getConfigTreeBuilder(): \Symfony\Component\Config\Definition\Builder\TreeBuilder
    {
        $treeBuilder = new Builder\TreeBuilder('functional');
        /** @var Builder\ArrayNodeDefinition $rootNode */
        $rootNode = $treeBuilder->getRootNode();

        $rootNode
            ->canBeDisabled()
            ->children()
                ->arrayNode('tests')
                    ->prototype('array')
                        ->prototype('array')
                            ->canBeDisabled()
                            ->children()
                                ->arrayNode('request')
                                    ->beforeNormalization()
                                        ->ifTrue(fn ($v): bool => !is_array($v))
                                        ->then(fn ($v): array => ['uri' => $v])
                                    ->end()
                                    ->children()
                                        ->scalarNode('uri')
                                            ->isRequired()
                                        ->end()
                                        ->enumNode('method')
                                            ->values([
                                                'GET', 'POST', 'PUT', 'PATCH', 'DELETE',
                                            ])
                                            ->defaultValue('GET')
                                        ->end()
                                        ->arrayNode('headers')
                                            ->prototype('variable')->end()
                                        ->end()
                                    ->end()
                                ->end()
                                ->append($this->getAssertNode())
                            ->end()
                        ->end()
                    ->end()
                ->end()
                ->arrayNode('disabled')
                    ->prototype('variable')->end()
                ->end()
            ->end();

        return $treeBuilder;
    }
}
