<?php

namespace GetRepo\PHPUnitYaml\Configuration;

use Symfony\Component\Config\Definition\Builder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
use Symfony\Component\Finder\Finder;

/**
 * AbstractTestConfiguration.
 */
abstract class AbstractTestConfiguration implements ConfigurationInterface
{
    protected function getAssertNode(): Builder\ArrayNodeDefinition
    {
        $node = new Builder\ArrayNodeDefinition('asserts');

        // dynamic assert names
        $finder = new Finder();
        $finderFiles = $finder->files()->in(\dirname(__DIR__) . '/Assert')->name('*.php');
        $asserts = [];
        foreach ($finderFiles as $fileInfo) {
            $filename = $fileInfo->getFilenameWithoutExtension();
            if (!str_contains($filename, 'Abstract')) {
                /** @var \Symfony\Component\Finder\SplFileInfo $fileInfo */
                $asserts[] = \lcfirst($filename);
            }
        }

        return $node
            ->requiresAtLeastOneElement()
            ->prototype('array')
                // if Expression language is set without array
                ->beforeNormalization()
                    ->ifTrue(fn ($value): bool => is_scalar($value))
                    ->then(fn ($value): array => [
                        'is_expression' => true,
                        'expect' => $value,
                    ])
                ->end()
                // Expression language validation
                ->validate()
                    ->ifTrue(fn ($child): bool => $child['is_expression'] && !is_scalar($child['expect']))
                    ->thenInvalid('"expect" value is not an expression language (scalar)')
                ->end()
                ->children()
                    ->booleanNode('is_expression')
                        ->defaultTrue()
                    ->end()
                    ->variableNode('expect')
                        ->isRequired()
                    ->end()
                    ->scalarNode('path')
                        ->defaultNull()
                    ->end()
                    ->enumNode('type')
                        ->values($asserts)
                        ->defaultValue('equals')
                    ->end()
                ->end()
            ->end();
    }

    protected function getMocksNode(): Builder\ArrayNodeDefinition
    {
        $node = new Builder\ArrayNodeDefinition('mocks');

        return $node
            ->prototype('array')
                ->children()
                    ->scalarNode('class')
                        ->isRequired()
                        ->cannotBeEmpty()
                    ->end()
                    ->variableNode('options')
                        ->validate()
                            ->ifTrue(function ($v): bool {
                                $diff = array_diff(array_keys((array) $v), [
                                    'disableOriginalConstructor',
                                    'disableOriginalClone',
                                    'disableArgumentCloning',
                                    'disallowMockingUnknownTypes',
                                    'setConstructorArgs',
                                    'setMockClassName',
                                ]);

                                return count($diff) > 0;
                            })
                            ->thenInvalid('Invalid mock options in \'%s\'')
                        ->end()
                    ->end()
                    ->arrayNode('methods')
                        ->prototype('array')
                            ->prototype('array')
                                ->children()
                                    ->arrayNode('params')
                                        ->prototype('variable')->end()
                                    ->end()
                                    ->variableNode('return')
                                        ->isRequired()
                                    ->end()
                                    // @TODO set up 'expects' option
                                    // 'at 2', 'at 2,3,4', 'from 2', 'until 3'
                                    /*
                                    ->scalarNode('expects')
                                        ->defaultNull()
                                    ->end()
                                    */
                                ->end()
                            ->end()
                        ->end()
                    ->end()
                    ->arrayNode('properties')
                        ->prototype('variable')->end()
                    ->end()
                ->end()
            ->end();
    }

    protected function getVariablesNode(): ?\Symfony\Component\Config\Definition\Builder\NodeParentInterface
    {
        $node = new Builder\ArrayNodeDefinition('variables');

        return $node->prototype('variable')->end();
    }

    protected function getDataNode(): ?\Symfony\Component\Config\Definition\Builder\NodeParentInterface
    {
        $node = new Builder\ArrayNodeDefinition('data');

        return $node->prototype('variable')->end();
    }
}
