<?php

namespace GetRepo\PHPUnitYaml\Resolver;

use PHPUnit\Framework\MockObject\MockBuilder as PHPUnitMockBuilder;
use PHPUnit\Framework\MockObject\MockObject;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * class MocksResolver.
 */
class MocksResolver extends AbstractResolver
{
    final public const PHPUNIT_BUILDER_CLASSNAME = 'PHPUnitYAML_MockBuilder_TestCase';

    public static function getPriority(): int
    {
        return 100;
    }

    public function buildValue(mixed &$value, ParameterBagInterface $bag): MockObject
    {
        return $value = $this->createMock($value, $bag);
    }

    /**
     * @SuppressWarnings("EvalExpression")
     */
    private function createMock(array $values, ParameterBagInterface $bag): MockObject
    {
        // create test case once
        $testCase = self::PHPUNIT_BUILDER_CLASSNAME;
        if (!class_exists($testCase)) {
            eval("class {$testCase} extends \PHPUnit\Framework\TestCase {};");
        }
        $testCase = new $testCase();

        $className = is_object($values['class']) ? $values['class']::class : $values['class'];
        $mockBuilder = $testCase->getMockBuilder($className);

        // mock options
        if (isset($values['options']) && $values['options']) {
            foreach ($values['options'] as $method => $args) {
                $this->setMockOption($mockBuilder, $method, $bag, $args);
            }
        }

        // mock builder
        $mocked = $mockBuilder->getMock();

        // mock methods
        if (isset($values['methods']) && $values['methods']) {
            foreach ($values['methods'] as $method => $confs) {
                $this->setMockMethod($testCase, $mocked, $method, $confs, $bag);
            }
        }

        // mock properties
        if (isset($values['properties']) && $values['properties']) {
            foreach ($values['properties'] as $property => $value) {
                $mocked->{$property} = $this->resolveValue($bag, $value);
            }
        }

        return $mocked;
    }

    private function setMockOption(
        PHPUnitMockBuilder &$mockBuilder,
        string $method,
        ParameterBagInterface $bag,
        array $args = null
    ): void {
        if ('setConstructorArgs' === $method) {
            $args = [(array) $args];
        }
        $callback = [$mockBuilder, $method];
        if (!is_callable($callback)) {
            throw new \Exception("Mock method {$method} does not exists");
        }
        call_user_func_array(
            [$mockBuilder, $method],
            (array) $args
        );
    }

    private function setMockMethod(
        \PHPUnitYAML_MockBuilder_TestCase $test,
        object &$mocked,
        string $method,
        array $confs,
        ParameterBagInterface $bag
    ): void {
        // @TODO set up expects 'any', 'once' from conf
        $mocked->expects($test->any())
            ->method($method)
            ->will($test->returnCallback(function () use ($confs, $method, $bag, $mocked) {
                $params = func_get_args();
                // create parameters mapping, by key and by name
                $rMethod = new \ReflectionMethod($mocked, $method);
                $parametersMapping = [];
                /** @var \ReflectionParameter $parameter */
                foreach ($rMethod->getParameters() as $i => $parameter) {
                    $parameterMap = [
                        'optional' => $parameter->isOptional(),
                        'default' => $parameter->isOptional() ? $parameter->getDefaultValue() : null,
                    ];
                    $parametersMapping['by_key'][$i] = $parameterMap;
                    $parameterMap['key'] = $i;
                    $parametersMapping['by_name'][$parameter->name] = $parameterMap;
                }

                foreach ($confs as $conf) {
                    $paramsMatch = true;
                    if (isset($conf['params'])) {
                        foreach ($conf['params'] as $paramKey => $paramValue) {
                            // resolve parameter name to int key
                            if (!\is_int($paramKey)) {
                                if (!isset($parametersMapping['by_name'][$paramKey])) {
                                    throw new \Exception(\sprintf(
                                        'Argument "%s" of mocked method "%s()" does not exists',
                                        $paramKey,
                                        $method
                                    ));
                                }
                                $paramKey = $parametersMapping['by_name'][$paramKey]['key'];
                            }
                            // check value is same or different
                            if (
                                !\array_key_exists($paramKey, $params)
                                || $paramValue !== $params[$paramKey]
                            ) {
                                $paramsMatch = false;
                                break;
                            }
                        }
                    }

                    if ($paramsMatch) {
                        if (\is_string($conf['return']) && str_starts_with($conf['return'], '%')) {
                            $key = trim($conf['return'], '%');
                            $conf['return'] = $bag->has($key) ? $bag->get($key) : $conf['return'];
                        }

                        return $this->resolveValue($bag, $conf['return']);
                    }
                }

                return null;
            }));
    }
}
