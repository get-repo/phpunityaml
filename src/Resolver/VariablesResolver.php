<?php

namespace GetRepo\PHPUnitYaml\Resolver;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * class VariablesResolver.
 */
class VariablesResolver extends AbstractResolver
{
    public static function getPriority(): int
    {
        return 1;
    }

    /**
     * @return mixed
     */
    public function buildValue(mixed &$value, ParameterBagInterface $bag)
    {
        return $this->resolveValue($bag, $value);
    }
}
