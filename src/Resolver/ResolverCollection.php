<?php

namespace GetRepo\PHPUnitYaml\Resolver;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\ParameterNotFoundException;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\String\Inflector\EnglishInflector;

/**
 * Resolver.
 */
class ResolverCollection
{
    final public const MAX_ITERATIONS = 5;

    private array $resolvers = [];

    private array $failed = [];

    public function __construct(private readonly ParameterBagInterface $bag, RewindableGenerator $resolvers = null)
    {
        // create resolver array by alias
        if ($resolvers) {
            foreach ($resolvers->getIterator() as $name => $resolver) {
                $this->resolvers[$name] = $resolver;
            }
        }
    }

    public static function getDefaultKeyName(string $key = null): string
    {
        // makes DI tags key happy
        return $key ? "phpunityaml.resolver.{$key}" : 'phpunityaml.resolver';
    }

    /**
     * @return AbstractResolver|false
     */
    public function findOne(string $name)
    {
        $inflector = new EnglishInflector();
        $plural = current($inflector->pluralize($name));
        $guesses = [
            $name, // full name
            "phpunityaml.resolver.{$name}", // singular
            "phpunityaml.resolver.{$plural}", // plural
        ];

        foreach ($guesses as $guess) {
            if (isset($this->resolvers[$guess])) {
                return $this->resolvers[$guess];
            }
        }

        return false;
    }

    public function resolveValues(array $values): array
    {
        $this->failed = []; // reset
        $bag = clone $this->bag;

        // first pass to resolve bag values only
        $tmp = $values;
        foreach ($tmp as $key => &$value) {
            $value = $this->resolveValue($bag, $key, $value, $values);
        }

        $bag->set('app_root_path', APP_ROOT_PATH);
        $bag->resolve();

        return $bag->resolveValue($values);
    }

    /**
     * @return mixed
     */
    private function resolveValue(ParameterBagInterface $bag, string $key, mixed $value, array $values)
    {
        if ($value && ($resolver = $this->findOne($key))) {
            try {
                return $resolver->build($bag, $value);
            } catch (ParameterNotFoundException $e) {
                @$this->failed[$e->getKey()]++;
                if ($this->failed[$e->getKey()] > self::MAX_ITERATIONS) {
                    throw new \LogicException(\sprintf("Key '%s' could not be resolved", $e->getKey()));
                }
                $parts = explode('.', $e->getKey());
                if (2 === count($parts)) {
                    [$depKey, $depName] = $parts;
                    $depResolver = $this->findOne($depKey);
                    $depKey = $depResolver->getName();
                    if (isset($values[$depKey][$depName])) {
                        $this->resolveValue(
                            $bag,
                            $depResolver->getName(),
                            [$depName => $values[$depKey][$depName]],
                            $values
                        );

                        return $this->resolveValue($bag, $key, $value, $values);
                    }
                }
            }
        }

        return $value;
    }
}
