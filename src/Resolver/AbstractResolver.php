<?php

namespace GetRepo\PHPUnitYaml\Resolver;

use GetRepo\PHPUnitYaml\Util\ParameterBagUtil;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\String\Inflector\EnglishInflector;

/**
 * class AbstractResolver.
 */
abstract class AbstractResolver
{
    /**
     * Priority of resolution.
     */
    abstract public static function getPriority(): int;

    /**
     * @return mixed
     */
    abstract protected function buildValue(mixed &$value, ParameterBagInterface $bag);

    public function __construct(private readonly string $name)
    {
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getBagName(string $key): string
    {
        $inflector = new EnglishInflector();
        $singulars = $inflector->singularize($this->name);

        return sprintf(
            '%s.%s',
            end($singulars),
            strtolower($key)
        );
    }

    /**
     * @return mixed[]
     */
    public function build(ParameterBagInterface $bag, array $values): array
    {
        if ($bag->all()) {
            $values = $bag->resolveValue($values);
        }

        foreach ($values as $k => &$v) {
            $builtValue = $this->buildValue($v, $bag);
            // Parameter bag does not handle object values, so we handle it manually
            ParameterBagUtil::setIfObject($builtValue);
            $bag->set($this->getBagName($k), $builtValue);
        }

        return $bag->resolveValue($values);
    }

    /**
     * @return mixed
     */
    protected function resolveValue(ParameterBagInterface $bag, mixed $value)
    {
        if (\is_string($value) && str_starts_with($value, '%')) {
            $key = trim($value, '%');
            $value = $bag->has($key) ? $bag->get($key) : $value;
        } elseif (\is_array($value) && $value) {
            $value = $bag->resolveValue($value);
        }

        // Parameter bag does not handle object values, so we handle it manually
        ParameterBagUtil::getIfObject($value);

        return $value;
    }
}
