<?php

namespace GetRepo\PHPUnitYaml\Assert;

use PHPUnit\Framework\Assert;

abstract class AbstractAssert extends Assert
{
    abstract protected function doAssert(mixed $expected, mixed $actual): void;

    public function assert(mixed $expected, mixed $actual): void
    {
        $this->checkExpectedType($expected);
        $this->checkActualType($actual);
        $this->doAssert($expected, $actual);
    }

    protected function checkExpectedType(mixed $expected): void
    {
        // override it if you want
    }

    protected function checkActualType(mixed $actual): void
    {
        // override it if you want
    }
}
