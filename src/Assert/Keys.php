<?php

namespace GetRepo\PHPUnitYaml\Assert;

class Keys extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        static::assertEquals($expected, \implode(',', array_keys($actual)));
    }

    protected function checkExpectedType(mixed $expected): void
    {
        static::assertIsString($expected, \sprintf(
            'Keys expected value must be type of string (comma separated), %s given',
            \gettype($expected)
        ));
    }

    protected function checkActualType(mixed $actual): void
    {
        static::assertIsArray($actual, \sprintf(
            'Keys actual value must be an array, %s given',
            \gettype($actual)
        ));
    }
}
