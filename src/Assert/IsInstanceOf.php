<?php

namespace GetRepo\PHPUnitYaml\Assert;

class IsInstanceOf extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        static::assertInstanceOf($expected, $actual);
    }

    protected function checkExpectedType(mixed $expected): void
    {
        static::assertIsString($expected, \sprintf(
            'IsInstanceOf expected value must be type of object, %s given',
            \gettype($expected)
        ));
    }

    protected function checkActualType(mixed $actual): void
    {
        static::assertIsObject($actual, \sprintf(
            'IsInstanceOf actual value must be type of object, %s given',
            \gettype($actual)
        ));
    }
}
