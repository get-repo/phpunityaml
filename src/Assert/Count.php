<?php

namespace GetRepo\PHPUnitYaml\Assert;

class Count extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        static::assertCount($expected, $actual);
    }

    protected function checkActualType(mixed $actual): void
    {
        static::assertIsIterable($actual, \sprintf(
            'Count actual value must be iterable, %s given',
            \gettype($actual)
        ));
    }
}
