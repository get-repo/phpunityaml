<?php

namespace GetRepo\PHPUnitYaml\Assert;

class Equals extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        static::assertEquals($expected, $actual);
    }
}
