<?php

namespace GetRepo\PHPUnitYaml\Assert;

class IsTypeOf extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        $method = $this->getAssertMethod($expected);
        $this->{$method}($actual);
    }

    protected function checkExpectedType(mixed $expected): void
    {
        static::assertTrue(
            \function_exists(sprintf('is_%s', \strtolower((string) $expected))),
            \sprintf('Variable type "%s" does not exists.', $expected)
        );
        // assert method exists
        $this->getAssertMethod($expected);
    }

    private function getAssertMethod(string $type): string
    {
        $method = sprintf('assertIs%s', \ucfirst($type));
        static::assertTrue(\method_exists($this, $method), \sprintf('Method "%s" does not exists.', $method));

        return $method;
    }
}
