<?php

namespace GetRepo\PHPUnitYaml\Assert;

class StartsWith extends AbstractAssert
{
    protected function doAssert(mixed $expected, mixed $actual): void
    {
        static::assertStringStartsWith($expected, $actual);
    }

    protected function checkExpectedType(mixed $expected): void
    {
        static::assertIsString($expected, \sprintf(
            'Keys expected value must be type of string, %s given',
            \gettype($expected)
        ));
    }

    protected function checkActualType(mixed $actual): void
    {
        static::assertIsString($actual, \sprintf(
            'Keys actual value must be type of string, %s given',
            \gettype($actual)
        ));
    }
}
