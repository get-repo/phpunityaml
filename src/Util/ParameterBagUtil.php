<?php

namespace GetRepo\PHPUnitYaml\Util;

/*
 * Parameter bag does not handle object values, so we handle it manually
 */
class ParameterBagUtil
{
    private const PARAMETER_BAG_OBJECT_KEY = '#!!!_____PARAMETER__BAG__OBJECT__KEY_____!!!#';

    public static function setIfObject(mixed &$value): void
    {
        if (is_object($value)) {
            $value = [ParameterBagUtil::PARAMETER_BAG_OBJECT_KEY => $value];
        }
    }

    public static function getIfObject(mixed &$value): void
    {
        if (is_array($value)) {
            $resolveObjects = function (array &$array) use (&$resolveObjects) {
                if (isset($array[self::PARAMETER_BAG_OBJECT_KEY])) {
                    $array = $array[self::PARAMETER_BAG_OBJECT_KEY];
                    return;
                }
                foreach ($array as &$v) {
                    if (is_array($v)) {
                        $resolveObjects($v);
                    }
                }
            };
            $resolveObjects($value);
        }
    }
}
