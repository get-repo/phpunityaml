<?php

namespace GetRepo\PHPUnitYaml\Util;

use Symfony\Bridge\Twig\Extension\FormExtension;
use Symfony\Bridge\Twig\Extension\TranslationExtension;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\Forms;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Validator\Validation;
use Twig\Environment as TwigEnvironment;
use Twig\Loader\FilesystemLoader as TwigFilesystemLoader;

class FormUtil
{
    /**
     * @see https://github.com/webmozart/standalone-forms/blob/2.7%2Btwig/src/setup.php
     */
    public function createFormFactory(ContainerInterface $container): FormFactoryInterface
    {
        if ($container->has('form.factory')) {
            return $container->get('form.factory');
        }

        $vendorDir = realpath(APP_ROOT_PATH . '/vendor');

        $validator = Validation::createValidator();

        $translator = new Translator('en');
        $translator->addLoader('xlf', new XliffFileLoader());
        foreach (['form', 'validator'] as $component) {
            $translator->addResource(
                'xlf',
                "{$vendorDir}/symfony/{$component}/Resources/translations/validators.en.xlf",
                'en',
                'validators'
            );
        }

        $twig = new TwigEnvironment(new TwigFilesystemLoader([
            realpath(APP_ROOT_PATH . '/templates'),
            "{$vendorDir}/symfony/twig-bridge/Resources/views/Form",
        ]));
        $twig->addExtension(
            new TranslationExtension($translator)
        );
        $twig->addExtension(new FormExtension());

        return Forms::createFormFactoryBuilder()
            ->addExtension(new ValidatorExtension($validator))
            ->getFormFactory();
    }

    public function getErrorMessages(FormInterface $form): array
    {
        $errors = [];

        foreach ($form->getErrors() as $error) {
            if ($form->isRoot()) {
                $errors['#'][] = $error->getMessage();
            } else {
                $errors[] = $error->getMessage();
            }
        }

        foreach ($form->all() as $child) {
            if (!$child->isSubmitted() || !$child->isValid()) {
                $errors[$child->getName()] = $this->getErrorMessages($child);
            }
        }

        return $errors;
    }
}
