<?php

namespace GetRepo\PHPUnitYaml\DependencyInjection;

use GetRepo\PHPUnitYaml\Assert\AbstractAssert;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * CompilerPass.
 */
class CompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        foreach ($container->findTags() as $tag) {
            $ids = $container->findTaggedServiceIds($tag);
            $callback = [
                $this,
                sprintf('handle%sDefinitions', ucfirst(\str_replace('phpunityaml.', '', $tag))),
            ];
            if (is_callable($callback)) {
                call_user_func($callback, array_keys($ids), $container);
            }
        }
    }

    private function handleAssertDefinitions(array $ids, ContainerBuilder $container): void // @phpstan-ignore-line
    {
        $availableAssert = [];
        foreach ($ids as $className) {
            $match = false;
            if (
                \in_array(AbstractAssert::class, class_parents($className))
                && preg_match('#\\\(\w+)$#', (string) $className, $match)
            ) {
                // get test type as service alias
                $assert = strtolower($match[1]);
                // create alias and public
                $container->setAlias("phpunityaml.assert.{$assert}", $className)->setPublic(true);
                $availableAssert[] = $assert;
            }
        }

        $container->setParameter('phpunityaml.asserts', $availableAssert);
    }

    private function handleRunnerDefinitions(array $ids, ContainerBuilder $container): void // @phpstan-ignore-line
    {
        $availableRunners = [];
        foreach ($ids as $className) {
            $match = false;
            if (preg_match('#\\\(\w+)TestRunner$#', (string) $className, $match)) {
                $definition = $container->getDefinition($className);
                // get test type as service alias
                $type = strtolower($match[1]);
                // model class parameter
                $modelClassParameter = "phpunityaml.model.class.{$type}";
                if (!$container->hasParameter($modelClassParameter)) {
                    $container->setParameter(
                        $modelClassParameter,
                        sprintf('GetRepo\PHPUnitYaml\Model\%sTestModel', ucfirst($type))
                    );
                }
                $definition->setArgument('$name', "%{$modelClassParameter}%");

                // Runners order
                $pos = call_user_func([$className, 'getOrder']);
                if (isset($availableRunners[$pos])) {
                    $message = sprintf(
                        "Runners '%s' and '%s' have the same position order '%d'",
                        $availableRunners[$pos],
                        $type,
                        $pos
                    );
                    throw new \LogicException($message);
                }
                // create alias and public
                $container->setAlias("phpunityaml.runner.{$type}", $className)->setPublic(true);
                // list of runners in parameter below
                $availableRunners[$pos] = $type;
            }
        }

        ksort($availableRunners);
        $container->setParameter('phpunityaml.runners', $availableRunners);
    }

    private function handleResolverDefinitions(array $ids, ContainerBuilder $container): void // @phpstan-ignore-line
    {
        $availableResolvers = [];
        foreach ($ids as $className) {
            $match = false;
            if (preg_match('#\\\(\w+)Resolver$#', (string) $className, $match)) {
                $definition = $container->getDefinition($className);
                // get test type as service alias
                $type = strtolower($match[1]);
                $alias = "phpunityaml.resolver.{$type}";
                // add key to tag
                $tags = $definition->getTags();
                $tags['phpunityaml.resolver'][0]['key'] = $alias;
                $definition->setTags($tags);
                // create alias and public
                $container->setAlias($alias, $className)->setPublic(true);
                // add name argument
                $definition->setArgument('$name', $type);
                // list of runners in parameter below
                $availableResolvers[] = $type;
            }
        }

        $container->setParameter('phpunityaml.resolvers', $availableResolvers);
    }
}
