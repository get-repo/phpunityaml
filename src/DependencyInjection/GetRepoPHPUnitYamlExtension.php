<?php

namespace GetRepo\PHPUnitYaml\DependencyInjection;

use GetRepo\PHPUnitYaml\Configuration\Configuration;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;

class GetRepoPHPUnitYamlExtension extends Extension
{
    /** @var string */
    final public const ALIAS = 'phpunityaml';

    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\DependencyInjection\Extension\Extension::getAlias()
     */
    public function getAlias(): string
    {
        return self::ALIAS;
    }

    /**
     * {@inheritdoc}
     *
     * @see \Symfony\Component\DependencyInjection\Extension\ExtensionInterface::load()
     */
    public function load(array $configs, ContainerBuilder $container): void
    {
        $config = $this->processConfiguration(new Configuration(), $configs);
        $container->setParameter(\sprintf('%s.config', self::ALIAS), $config);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(realpath(__DIR__ . '/../../'))
        );

        $loader->load('services.yml');
    }
}
