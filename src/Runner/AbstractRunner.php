<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\ExpressionLanguage\ExpressionLanguage;
use GetRepo\PHPUnitYaml\Model\ModelInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Config\Definition\Processor;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Finder\Finder;
use Symfony\Component\PropertyAccess as PA;
use Symfony\Component\Yaml\Yaml;

/**
 * AbstractRunner.
 */
abstract class AbstractRunner extends TestCase implements RunnerInterface
{
    protected static ?\Symfony\Component\DependencyInjection\Container $container = null;

    protected PA\PropertyAccessorInterface $propertyAccessor;

    private static string $testPath = APP_ROOT_PATH . '/tests';

    private static string $fileFilter = '';

    private static string $modelClassName = '';

    abstract protected function doTestRun(ModelInterface $test): void;

    /**
     * @param string $name
     * @param mixed ...$params
     */
    public function __construct(string $name = 'testRun', ...$params)
    {
        if (class_exists($name) && is_subclass_of($name, ModelInterface::class)) {
            self::$modelClassName = $name;
            $name = 'testRun';
        }

        $builder = PA\PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->disableMagicCall();

        $builder->enableExceptionOnInvalidPropertyPath();

        $this->propertyAccessor = $builder->getPropertyAccessor();

        parent::__construct($name, ...$params);
    }

    public function canBeExecuted(): bool
    {
        return true;
    }

    public function setContainer(Container $container): self
    {
        self::$container = $container;

        return $this;
    }

    public function setTestPath(string $testPath): self
    {
        self::$testPath = $testPath;

        return $this;
    }

    public function setFileFilter(string $filter): self
    {
        self::$fileFilter = $filter;

        return $this;
    }

    /**
     * @return bool[]|string[]
     */
    public function getTestFiles(): array
    {
        $files = [];
        $extension = $this->getTestFileExtension();
        $finder = (new Finder())->files()
            ->in(self::$testPath)
            ->name("*{$extension}")
            ->followLinks();

        foreach ($finder as $file) {
            // apply file filter
            if (self::$fileFilter && (false === stripos($file, self::$fileFilter))) {
                continue;
            }

            $files[] = $file->getRealPath();
        }

        return $files;
    }

    /**
     * {@inheritdoc}
     *
     * @see RunnerInterface::getTestFileExtension()
     */
    public function getTestFileExtension(): string
    {
        $match = false;
        if (preg_match('/\\\(\w+)TestRunner$/', $class = static::class, $match)) {
            return sprintf('%s.test.yml', strtolower($match[1]));
        }

        throw new \LogicException("Could not guess test file extension from class {$class}");
    }

    /**
     * @return array
     */
    public function runProvider(): array
    {
        return $this->buildProvider(function (&$data, $modelClass, $file, $testName, $assertName, $test, $yaml) {
            $name = basename($file) . " / {$testName} / {$assertName}";
            $test['yaml_file'] = $file;
            $test['enabled'] = $yaml['enabled'] ? $test['enabled'] : false;
            $data[$name] = [new $modelClass($name, $test)];
        });
    }

    /**
     * @dataProvider runProvider
     */
    public function testRun(ModelInterface $test): void
    {
        if ($test->get('[enabled]')) {
            try {
                $this->doTestRun($test);
            } catch (\Exception $e) {
                throw $e;
            }
        } else {
            $this->markTestSkipped();
        }
    }

    protected function doAssertions(mixed $actual, array $asserts, array $extraExpressionValues = []): void
    {
        if ($actual instanceof \PHPUnit\Framework\Error\Error) {
            $this->assertTrue(
                false,
                \sprintf(
                    '%s in %s:%d',
                    $actual->getMessage(),
                    \basename($actual->getFile()),
                    $actual->getLine()
                )
            );
        }

        // also avoid phpunit message "This test did not perform any assertions"
        $this->assertNotEmpty($asserts, 'no asserts');

        $expressionLanguage = new ExpressionLanguage();
        $differ = self::$container->get('phpunityaml.differ');

        foreach ($asserts as $assert) {
            $assertActual = $actual;

            if ($assert['is_expression']) {
                $values = array_merge(['actual' => $assertActual], $extraExpressionValues);
                if ($assert['path']) {
                    $assertActual = $this->propertyAccessor->getValue($assertActual, $assert['path']);
                }
                // expression
                $expression = $assert['expect'];
                // evaluate expression, Must return true to success.
                // @ for silent notices on EL arrays keys doe not exists.
                $expect = (bool) @$expressionLanguage->evaluate($expression, $values);
                if (true !== $expect) {
                    // create diff output
                    $this->assertTrue(
                        $expect,
                        // @ for silent notices on EL arrays keys doe not exists.
                        @$differ->diff($expression, $values)
                    );
                }
            } else {
                if ($assert['path']) {
                    $assertActual = $this->propertyAccessor->getValue($assertActual, $assert['path']);
                }

                self::$container->get(\sprintf('phpunityaml.assert.%s', \strtolower((string) $assert['type'])))
                    ->assert($assert['expect'], $assertActual);
            }
        }
    }

    /**
     * @return mixed
     */
    protected function getConfig(string $path = null)
    {
        $config = (array) self::$container->getParameter('phpunityaml.config');

        return $path ? $this->propertyAccessor->getValue($config, $path) : $config;
    }

    protected function buildProvider(callable $callbacks): array
    {
        try {
            if (!self::$modelClassName) {
                throw new \RuntimeException('No valid model classname defined');
            }

            $data = [];
            $files = $this->getTestFiles();
            $modelClass = self::$modelClassName;
            $configClass = call_user_func([$modelClass, 'getConfigurationClassName']);
            $resolverCollection = self::$container->get('phpunityaml.resolver_collection');

            foreach ($files as $file) {
                $yaml = Yaml::parseFile($file, Yaml::PARSE_DATETIME);
                // validate yaml content
                $yaml = (new Processor())->processConfiguration(
                    new $configClass(),
                    [$yaml]
                );
                // create a model for each tests
                if (isset($yaml['tests']) && $yaml['tests']) {
                    foreach ((array) $yaml['tests'] as $testName => $tests) {
                        foreach ($tests as $assertName => $test) {
                            $test = $resolverCollection->resolveValues($test);
                            $callbacks(
                                $data, // reference must be used
                                $modelClass,
                                $file,
                                $testName,
                                $assertName,
                                $test,
                                $yaml
                            );
                        }
                    }
                }
            }
        } catch (\Exception $e) {
            // TODO create specific exception to throw in bin
            throw $e;
        }

        return $data;
    }
}
