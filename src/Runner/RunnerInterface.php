<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\PHPUnitYaml\Model\ModelInterface;

/**
 * RunnerInterface.
 */
interface RunnerInterface
{
    /** @return int */
    public static function getOrder(): int;

    /** @return array */
    public function runProvider(): array;

    /** @param ModelInterface $test */
    public function testRun(ModelInterface $test): void;
}
