<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\PHPUnitYaml\Model\ModelInterface;
use GetRepo\PHPUnitYaml\Util\FormUtil;

/**
 * FormTestRunner.
 */
class FormTestRunner extends AbstractRunner
{
    /**
     * {@inheritdoc}
     *
     * @see \GetRepo\PHPUnitYaml\Runner\RunnerInterface::getOrder()
     */
    public static function getOrder(): int
    {
        return 3;
    }

    /**
     * @dataProvider provider
     * @param \GetRepo\PHPUnitYaml\Model\FormTestModel $test
     */
    protected function doTestRun(ModelInterface $test): void
    {
        /** @var FormUtil $formUtil */
        $formUtil = self::$container->get(FormUtil::class);

        // data object (mocked)
        $dataObject = $test->get('[options][data]', false);
        // create form
        $form = $formUtil->createFormFactory(self::$container)
            ->create(
                $test->get('[class]'),
                \is_object($dataObject) ? $dataObject : null,
                $test->get('[options]')
            );
        // submit form
        $form->submit($test->get('[data]'));

        $this->doAssertions(
            $data = $form->getData(),
            $test->get('[asserts]'),
            [
                'data' => $data,
                'form' => $form,
                'config' => $form->getConfig(),
                'valid' => $form->isValid(),
                'errors' => $formUtil->getErrorMessages($form),
                'view' => $form->createView()->children,
            ]
        );
    }
}
