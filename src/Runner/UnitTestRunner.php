<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\PHPUnitYaml\Model\ModelInterface;

/**
 * UnitTestRunner.
 */
class UnitTestRunner extends AbstractRunner
{
    use StrictTestTrait;

    /**
     * {@inheritdoc}
     *
     * @see \GetRepo\PHPUnitYaml\Runner\RunnerInterface::getOrder()
     */
    public static function getOrder(): int
    {
        return 1;
    }

    /**
     * @dataProvider provider
     * @param \GetRepo\PHPUnitYaml\Model\UnitTestModel $test
     */
    protected function doTestRun(ModelInterface $test): void
    {
        /** @var \GetRepo\PHPUnitYaml\Model\UnitTestModel $test */
        // reflection method
        $rMethod = $test->getReflectionMethod();
        // method parameters
        $parameters = (array) $test->get('[parameters]', false);
        // reflection class
        $rClass = $test->getReflectionClass();

        // method must be public to be tested
        $this->assertTrue(
            $rMethod->isPublic(),
            sprintf('Method %s::%s() is not callable.', $rClass->name, $rMethod->name)
        );

        // create class instance based on method type
        if ($rMethod->isConstructor()) {
            $rClass = new \ReflectionClass($rMethod->class);
            $actual = $rClass->newInstanceArgs($parameters);
        } else {
            $callback = [$rClass->name, $rMethod->name];
            if (!$rMethod->isStatic()) {
                // replace static class name by object
                $callback[0] = $rClass->newInstanceArgs(
                    (array) $test->get('[constructor]', false)
                );
            }
            try {
                $actual = call_user_func_array($callback, $parameters);
            } catch (\Exception $e) {
                $actual = $e;
            }
        }

        $this->doAssertions($actual, $test->get('[asserts]'));
    }
}
