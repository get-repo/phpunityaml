<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\PHPUnitYaml\Model\ModelInterface;

/**
 * EntityTestRunner.
 */
class EntityTestRunner extends AbstractRunner
{
    use StrictTestTrait;

    /**
     * {@inheritdoc}
     *
     * @see UnitTestRunner::getOrder()
     */
    public static function getOrder(): int
    {
        return 2;
    }

    /**
     * @dataProvider provider
     * @param \GetRepo\PHPUnitYaml\Model\UnitTestModel $test
     */
    protected function doTestRun(ModelInterface $test): void
    {
        /** @var \GetRepo\PHPUnitYaml\Model\UnitTestModel $test */
        // reflection method
        $rMethod = $test->getReflectionMethod();
        // method parameters
        $parameters = (array) $test->get('[parameters]', false);
        // reflection class
        $rClass = $test->getReflectionClass();

        if ($rMethod->isConstructor()) {
            throw new \LogicException("Could not test constructor of class {$rClass->name}");
        }
        if ($rMethod->isStatic()) {
            throw new \LogicException("Could not test static method {$rClass->name}::{$rMethod->name}()");
        }

        // method must be public to be tested
        $this->assertTrue(
            $rMethod->isPublic(),
            sprintf('Method %s::%s() is not callable.', $rClass->name, $rMethod->name)
        );

        $rConstructor = $rClass->getConstructor();
        $constructorParameters = (array) $test->get('[constructor]', false);
        if ($rConstructor && $rConstructor->getNumberOfRequiredParameters()) {
            // check for missing constructor parameters
            foreach ($rConstructor->getParameters() as $rParameter) {
                $rParameterName = $rParameter->getName();
                if (!$rParameter->isOptional() && !array_key_exists($rParameterName, $constructorParameters)) {
                    throw new \LogicException("Missing constructor parameter \"{$rParameterName}\"");
                }
            }
        }
        // replace static class name by object
        $entity = $rClass->newInstanceArgs($constructorParameters);
        // set data entity
        foreach ($test->get('[data]') as $propertyPath => $data) {
            $this->propertyAccessor->setValue($entity, $propertyPath, $data);
        }

        try {
            $actual = call_user_func_array([$entity, $rMethod->name], $parameters);
        } catch (\Exception $e) {
            $actual = $e;
        }

        $this->doAssertions($actual, $test->get('[asserts]'));
    }
}
