<?php

namespace GetRepo\PHPUnitYaml\Runner;

use Symfony\Component\Yaml\Yaml;

trait StrictTestTrait
{
    /**
     * @return array
     */
    public function strictProvider(): array
    {
        try {
            $data = [];
            $files = $this->getTestFiles();
            foreach ($files as $file) {
                $yaml = Yaml::parseFile($file, Yaml::PARSE_DATETIME);
                if ($yaml['strict'] ?? false) {
                    $actualMethods = [];
                    foreach ($yaml['tests'] ?? [] as $methodName => $tests) {
                        if ($tests) {
                            $actualMethods[] = $methodName;
                        }
                    }
                    $data["strict tests {$yaml['class']}"] = [
                        $yaml['class'],
                        $actualMethods
                    ];
                }
            }
        } catch (\Exception $e) {
            // TODO create specific exception to throw in bin
            throw $e;
        }

        // if empty, makes sure testStrict() pass at least once to avoid yellow message
        if (!$data) {
            $data = [['__STRICT_TEST_DISABLED__', []]];
        }

        return $data;
    }

    /**
     * @dataProvider strictProvider
     */
    public function testStrict(string $modelClass, array $actualTestedMethods): void
    {
        // if empty, makes sure testStrict() pass at least once to avoid yellow message
        if ('__STRICT_TEST_DISABLED__' === $modelClass) {
            $this->assertEmpty($actualTestedMethods);
            return;
        }

        $rClass = new \ReflectionClass($modelClass);
        $expectedTestedMethods = [];
        /** @var \ReflectionMethod $rMethod */
        foreach ($rClass->getMethods() as $rMethod) {
            if ($rMethod->isPublic() && !$rMethod->isAbstract() && '__construct' !== $rMethod->name) {
                $expectedTestedMethods[] = $rMethod->name;
            }
        }

        $this->assertEquals(
            [],
            \array_diff($expectedTestedMethods, $actualTestedMethods),
            "Missing methods do be tested for class {$modelClass}"
        );
    }
}
