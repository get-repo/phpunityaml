<?php

namespace GetRepo\PHPUnitYaml\Runner;

use GetRepo\PHPUnitYaml\Model\ModelInterface;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;

/**
 * FunctionalTestRunner.
 */
class FunctionalTestRunner extends AbstractRunner
{
    /**
     * {@inheritdoc}
     *
     * @see \GetRepo\PHPUnitYaml\Runner\RunnerInterface::getOrder()
     */
    public static function getOrder(): int
    {
        return 4;
    }

    public function canBeExecuted(): bool
    {
        return !self::$container->hasParameter('standalone'); // only symfony bundle
    }

    /**
     * @dataProvider provider
     * @param \GetRepo\PHPUnitYaml\Model\FunctionalTestModel $test
     */
    protected function doTestRun(ModelInterface $test): void
    {
        $client = new KernelBrowser(
            self::$container->get('kernel'),
            $this->getConfig('[functional][server_parameters]')
        );
        $client->followRedirects(true);
        $crawler = $client->request(
            $test->get('[request][method]'),
            $test->get('[request][uri]')
        );

        $this->doAssertions(
            $crawler,
            $test->get('[asserts]'),
            ['client' => $client]
        );

        unset($client); // destruct
    }
}
