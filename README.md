<p align="center">
    <img src="https://assets.gitlab-static.net/uploads/-/system/project/avatar/11239520/LOGO_PHPNITYAML.png" height=100 />
</p>

<h1 align=center>PHPUNITYAML</h1>
<h4 align=center style="border-bottom: 3px solid #6E8DFF; padding-bottom: 15px;">
    Write PHPUnit test in YAML
</h4>

<br/>

This repo provides an independant testing framework based on [PHPUnit](https://github.com/sebastianbergmann/phpunit).<br/>
To build better more reliable applications with unit and functional tests in a fast way, all test are written in YAML language.

## Table of Contents

1. [Motto](#motto)
1. [Installation](#installation)
1. [Runner Command Line Reference](#runner-command-line-reference)
1. [Principles](#principles)
    1. [File extensions](#file-extensions)
    1. [YAML reference syntax](#yaml-reference-syntax)
    1. [Init overrides in each test](#init-overrides-in-each-test)
    1. [Test expression language values and available asserts](#test-expression-language-values-and-available-asserts)
1. [Unit Testing](#unit-testing)
    1. [Unit Testing YAML Reference](#unit-testing-yaml-reference)
1. [Entity Testing](#entity-testing)
    1. [Entity Testing YAML Reference](#entity-testing-yaml-reference)
1. [Form Testing](#form-testing)
    1. [Form Testing YAML Reference](#form-testing-yaml-reference)
1. [Functional Testing](#functional-testing)
    1. [Functional Testing YAML Reference](#functional-testing-yaml-reference)

<br/><br/>
## Motto

 - I am lazy and I hate doing the same task more than once.
 - Good developers hates tests. They tend to write bad tests.
 - I should not write more code to test my existing code.
 - A testing framwork should provide standards and patterns.
 - My code should be testable. If I can't test it, I did it wrong.
 - Yes there is tests for this repo! But written with good old PHPUnit.

<br/><br/>
## Installation

#### Install with composer
This is installable via [Composer](https://getcomposer.org/):

    composer config repositories.get-repo/phpunityaml git https://gitlab.com/get-repo/phpunityaml.git
    composer config repositories.get-repo/expression-language git https://gitlab.com/get-repo/expression-language.git
    composer require get-repo/phpunityaml

#### Enable the bundle
In *config/bundles.php* if not already done by Symfony already
```php
<?php
return [
    ...
    GetRepo\PHPUnitYaml\GetRepoPHPUnitYamlBundle::class => ['test' => true],
];
```

<br/><br/>
## Runner Command Line Reference

Binary is located in `vendor/bin/phpunityaml`

```help
phpuniyaml by Symfonyaml.

Usage:
  phpuniyaml [options] path

Options:
  --type -t <type>   Filter by type (unit, functional, form)
  --file -f <file>   Filter by filename (wildcard)

All other options will be treated as PHPUnit options
```

<br/><br/>
## Principles

### File extensions
Test YAML files need a specific extension to be called.<br/>
Unit tests: `*.unit.test.yml`<br/>
Functional tests: `*.functional.test.yml`


### YAML reference syntax

The **init** section is there to prepare your test. You can define constructor, mocks, static variables.

**monstructor**: Define the constructor in your class and is invoked when the test creates a new instance.

**mocks**: Define the mocks to replace an object with a test double that (optionally) returns configured return values is referred to as stubbing.

**variables**: Just define variables.

Test YAML files are using the symfony dependency injection parameters resolver.
There are special cases such as when you want, for instance, to use the %variable.name% variable to reuse some data in your tests.

You can use some of the definitions alreay set with the syntax: `%namespace.name%`
Example:
```yaml
variable:
    testvar: 123

# later on ...

tests:
    title:
        name_of_the_test:
            constructor: ['%variable.testvar%'] # will be constructor = [123]
````

### Init overrides in each test
You can redefine values from the **init** section in a specific test.

```yaml
init:
    constructor: [1, 2]

tests:
    title:
        name_of_the_test:
            constructor: [1, 2, 3] # overrides the constructor defined in ini section
            asserts:
              - 'actual == "blablabla"'
```

### Test expression language values and available asserts
There is 2 ways to asserts values:

#### Using the Expression Language

[The ExpressionLanguage component](https://gitlab.com/get-repo/expression-language) provides an engine that can compile and evaluate expressions.
An expression is a one-liner that returns a value (mostly, but not limited to, Booleans).

```yaml
# ...
    asserts:
      - 'actual == 123'
```

**Functions**: See the [expression providers](https://gitlab.com/get-repo/expression-language#using-expression-providers)

**Values for unit tests**:
```php
['actual' => 123]; // just the actual test value
```

**Values for form tests**:
```php
[
    'actual' => 123,
    'form' => $form, // form object
    'data' => $data, // form data
    'config' => $config, // form config
    'valid' => false, // boolean if form is valid
    'errors' => ['field' => 'error message'], // array form errors
    'view' => [], // array form field views
];
```

**Values for functional tests**:
```php
[
    'actual' => 123,
    'client' => $client, // http client
];
```

#### Using PHPUnit asserts

PHPUnit's assertion methods are declared static and can be invoked from any context.

These can be called in this section, using `is_expression: false`.

```yaml
# ...
    asserts:
      - 
          is_expression: false
          expect: "foo"
          type: equals
      - 
          is_expression: false
          expect: App\Entity\Book
          type: isInstanceOf
      - 
          is_expression: false
          expect: 45
          type: count
      - 
          is_expression: false
          expect: ["a", "b"]
          type: keys
```


<br/><br/><br/>
## Unit Testing

Unit test YAML files must have the file extension `*.unit.test.yml`.

### Unit Testing YAML Reference

```yaml
enabled: true # to enable this test

class: App\Class\To\Test # required
# strict mode : If you add a new method without test, it will fail, asking for a test for this method
strict: true

init:
    # constructor args
    constructor:
        - 'arg1'
        - '%mock.constructor_arg2%' # reference to the mock 'constructor_arg2' defined in mocks
    # some variables to re-use later on in this test
    variables:
        var1: 123
    # class mocks
    mocks:
        # Prototype
        constructor_arg2:
            class: App\Class\To\Mock # required
            options:
                # can be used to disable the call to the original class constructor
                disableOriginalConstructor: ~
                # can be used to disable the call to the original class clone constructor
                disableOriginalClone: ~
                # disables the cloning of arguments passed to mocked methods
                disableArgumentCloning: ~
                # disallow mocking unknown types
                disallowMockingUnknownTypes: ~
                # can be used to specify a class name for the generated test double class
                setMockClassName: ~
            # methods to mock
            methods:
                methodName:
                    -
                    # if param1 = 8, then returns false
                        params: [8]
                        return: false
                    -
                    # if param named "number" = 8 and param named "varName" = var1, then return 55
                        params:
                            number: 8
                            varName: var1
                        return: 55
                    # default return true
                    - {return: true}
            # properties to mock
            properties:
                propertyName: 12345

tests:
    title_of_the_test_suite:
        test_with_expression_language:
            asserts:
              - 'actual == "blablabla"'
        many_tests_with_expression_language_and_init_overrides:
            constructor: [123]
            asserts:
              - {expect: 'actual == "blablabla"'}
              - {expect: 'actual != "whataver"'}
        test_without_expression_language:
            asserts:
              - {is_expression: false, expect: 'blablabla'}

# to disabled some test (with copy paste)
disabled: ~
```




<br/><br/><br/>
## Entity Testing

Entity (data objects) test YAML files must have the file extension `*.entity.test.yml`.

### Entity Testing YAML Reference

```yaml
enabled: true # to enable this test

class: App\Entity\Test # required
# strict mode : If you add a new method without test, it will fail, asking for a test for this method
strict: true

init:
    # some variables to re-use later on in this test
    variables:
        var1: 123
    # optional constructor args (data objects dont use constructors much anyway...)
    constructor:
        - 'arg1'
        - '%variable.var1%' # reference to the variable 'var1' defined in variables
    # class data (use setters)
    data:
        name: 'This is the name'
        reference: 'This is the reference'

tests:
    # test the getter getReference()
    getReference: # method name
        name_of_tour_test:
            asserts:
              - 'actual == "This is the reference"'

# to disabled some test (with copy paste)
disabled: ~
```




<br/><br/>
## Form Testing

Test Symfony forms.
Form test YAML files must have the file extension `*.form.test.yml`.

### Form Testing YAML Reference

```yaml
enabled: false # to enable/disable all the tests
class: App\Form\TestType

tests:
    test_form_failures:
        test_extra_fields:
            enabled: true # to enable/disable this test
            data:
                extra_field: 'extra_field_value'
            asserts:
                - 'valid === false'
                - 'errors["#"] == ["This form should not contain extra fields."]'
        test_asserts:
            data:
                name: null
            asserts:
                - 'valid === false'
                - 'length(errors) === 3'
                - 'keys(errors) == ["name", "bla", "nb"]'
                - 'errors["name"] == ["This value should not be blank."]'
                - 'errors["bla"] == ["This value should not be blank."]'
                - 'errors["nb"] == ["This value should not be blank."]'
        test_valid:
            data:
                name: 'valid name'
                bla: 'blabla'
                nb: 5
            asserts:
                - 'valid === true'
```


<br/><br/>
## Functional Testing

Http Tests.
Form test YAML files must have the file extension `*.functional.test.yml`.

### Functional Testing YAML Reference

```yaml
enabled: false # to enable/disable all the tests
tests:
    test_suite_name:
        test_name:
            request:
                uri: 'file://%app_root_path%/tests/index.html'
            asserts:
                - 'actual.filter(".container").text() == "blabla"'
```

