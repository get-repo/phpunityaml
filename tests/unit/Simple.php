<?php

// phpcs:ignoreFile

namespace Test\unit;

/**
 * @SuppressWarnings("CamelCaseMethodName")
 */
class Simple
{
    public static function static_method(): string
    {
        return 'static value';
    }

    public static function static_method_with_optional_param(string $optional = 'default'): string
    {
        return "static {$optional}";
    }

    public static function static_method_with_mandatory_param(?string $mandatory): ?string
    {
        return $mandatory;
    }

    public function method(): string
    {
        return 'value';
    }

    public function method_with_optional_param(string $optional = 'default'): string
    {
        return $optional;
    }

    /**
     * @return mixed
     */
    public function method_with_mandatory_param(mixed $mandatory)
    {
        return $mandatory;
    }
}
