<?php

// phpcs:ignoreFile

namespace Test\unit;

class Variables
{
    public function __construct(private readonly string $value)
    {
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function testParameters(Simple $simple, Mock $mock, string $string = null): string
    {
        return sprintf(
            'C=%s, S=%s, M=%s, S=%s',
            $this->value,
            $simple->method_with_mandatory_param($this->value),
            $mock->mockSimple($simple),
            $string
        );
    }
}
