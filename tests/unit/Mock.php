<?php

// phpcs:ignoreFile

namespace Test\unit;

class Mock
{
    /**
     * @return int[]|string[]|null[]
     */
    public function mock(Constructor $constructor): array
    {
        return $constructor->getValues();
    }

    public function mockSimple(Simple $simple): string
    {
        return $simple->method();
    }

    public function mockGreatherThan10(Constructor $constructor, int $int = 0): bool
    {
        return $constructor->greaterThan10($int);
    }
}
