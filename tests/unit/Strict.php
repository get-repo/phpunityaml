<?php

// phpcs:ignoreFile

namespace Test\unit;

class Strict
{
    public function getMethod1(): string
    {
        return 'method 1';
    }

    public function getMethod2(): string
    {
        return 'method 2';
    }

    protected function protectedMethodNotTested(): string
    {
        // just to make PHPStan happy (avoir privateMethodNotTested() is unused)
        $this->privateMethodNotTested();

        return 'protected method not tested 2';
    }

    private function privateMethodNotTested(): string
    {
        // just to make PHPStan happy (avoir protectedMethodNotTested() is unused)
        $this->protectedMethodNotTested();

        return 'protected method not tested 2';
    }
}
