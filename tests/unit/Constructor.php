<?php

// phpcs:ignoreFile

namespace Test\unit;

class Constructor
{
    public function __construct(private readonly int $int, private readonly ?string $string = null)
    {
    }

    /**
     * @return int[]|string[]|null[]
     */
    public function getValues(): array
    {
        return [$this->int, $this->string];
    }

    public function greaterThan10(int $int): bool
    {
        return $int > 10;
    }
}
