<?php

namespace Test\entity;

class Entity
{
    private string $client = '';
    private ?self $object = null;

    public function setClient(string $client): self
    {
        $this->client = $client;

        return $this;
    }

    public function getClient(string $prefix = null): string
    {
        return sprintf('%s%s', $prefix ? "{$prefix}_" : '', $this->client);
    }

    public function setObject(?self $object): self
    {
        $this->object = $object;

        return $this;
    }

    public function getObject(): ?self
    {
        return $this->object;
    }
}
