<?php

// phpcs:ignoreFile

namespace Test\form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints as Assert;

class SimpleForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        parent::buildForm($builder, $options);
        $builder
            ->add('int_gt_10', Type\IntegerType::class, [
                'constraints' => [
                    new Assert\NotNull(),
                    new Assert\GreaterThan(10),
                ],
            ]);
    }
}
