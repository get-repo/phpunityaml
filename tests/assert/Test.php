<?php

// phpcs:ignoreFile

namespace Test\assert;

class Test
{
    /**
     * @return int[]
     */
    public function getValues(): array
    {
        return [1, 2, 3];
    }

    public function getThis(): self
    {
        return $this;
    }

    public function getClassname(): string
    {
        return self::class;
    }
}
